#! /home/jo/.guix-profile/bin/guile -s
!#
(display ( ;; This procedure output it to stdou
          with-input-from-file (º ;; This procedure reads the file
                                car ( ;; This takes the first element of the list
                                     cdr ;; This takes the tail of the list
                                     ( command-line ) ;; This procedures reads all arguments passed and forms a list
                                     ))
  (lambda ()
    (reverse-list->string
     (let loop ((char (read-char))
                (result '()))
       (if (eof-object? char)
           result
           (loop (read-char) (cons char result))))))))
